package ch.shinungo.webform.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitterReturnValueHandler;

import ch.shinungo.webform.model.Customer;
import ch.shinungo.webform.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomerService {
	
	
	private List<Customer> customer = new ArrayList<Customer>();

	@Autowired
	private CustomerRepository customerRepo; 

	public Customer findCustomerByName(String name) {
		return customerRepo.findByName(name); 
	}
	
	public List<Customer> getAllCustomers(){
		
		Iterable<Customer>itCustomer = customerRepo.findAll(); 
		List<Customer> customerList = new ArrayList<Customer>();
		
		for (Customer customer : itCustomer) {
			customerList.add(customer); 
		}
	return customerList; 

	}
}
