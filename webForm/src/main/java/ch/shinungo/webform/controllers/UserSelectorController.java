package ch.shinungo.webform.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import ch.shinungo.webform.model.Customer;
import ch.shinungo.webform.service.UserForm;
import ch.shinungo.webform.service.UserService;
import lombok.Data;

@Controller
@Data
public class UserSelectorController {

	@Autowired
	private UserService userService;

	@GetMapping({ "getConsentId", "/", "/start", "/home", "userSelector" })

	public String showUserSelector(Model model) {
		  model.addAttribute("greeting", new Customer());
		return "sites/userSelector";
	}

	 // Wenn jemand was Post'et
	  @PostMapping("/greeting")
	  public String greetingSubmit(@ModelAttribute Customer greeting, Model model) {
	    model.addAttribute("greeting", greeting);
	    return "sites/showAccounts";
	  }	
	
}
