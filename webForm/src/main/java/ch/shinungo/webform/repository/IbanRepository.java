package ch.shinungo.webform.repository;

import org.springframework.data.repository.CrudRepository;

import ch.shinungo.webform.model.Iban;

public interface IbanRepository extends CrudRepository<Iban, Long> {

	public Iban findByIbannumber(String ibannumber);

}
