package ch.shinungo.webform.repository;

import org.springframework.data.repository.CrudRepository;

import ch.shinungo.webform.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{

	public Customer findByName(String name);
}
