package ch.shinungo.webform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebForm {

	public static void main(String[] args) {
		SpringApplication.run(WebForm.class, args);
	}
	
	// TEST FOR GIT
}